import numpy as np
import pandas as pd
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from os import listdir

mpl.rcParams['font.size']=18
mpl.rcParams['figure.figsize']=(10,5)
mpl.rcParams['lines.linewidth']=3

alllosses = {}
lossdir = 'losses/'
runtimes = []
for lossfile in listdir(lossdir):
    # Load file
    losses = np.load(lossdir+lossfile)
    runtime = losses[-1]
    losses = losses[:-1]
    Nepos = losses.shape[0]
    Nlay = int(lossfile.split('-')[0])
    Nhid = int(lossfile.split('-')[1])
    xp = lossfile.split('-')[2]
    yp = lossfile.split('-')[3]
    r = lossfile.split('-')[4]
    Lbatch = int(lossfile.split('-')[5])
    Nbatch = int(lossfile.split('-')[6])
    Nrepeat = int(lossfile.split('-')[7])
    tstall = int(lossfile.split('-')[8])
    seed = int(lossfile.split('-')[-1].split('.')[0])
    alllosses[Nlay,Nhid,xp,yp,r,Lbatch,Nbatch,Nrepeat,tstall,seed] = [losses]
    runtimes.append([Nlay,Nhid,xp,yp,r,Lbatch,Nbatch,Nrepeat,tstall,seed,runtime/Nepos])
    # # Plot loss vs epochs
    # plt.semilogy(losses)
    # plt.xlabel('Epoch')
    # plt.ylabel('Loss')
    # plt.title('Runtime was %.1f seconds per epoch'%(runtime/Nepos))
    # plt.tight_layout()
    # plt.savefig('plots/loss-'+lossfile.replace('npy','png'),dpi=200)
    # plt.close()


# Look at runtimes
df_run = pd.DataFrame(runtimes,columns=['Nlay','Nhid','xp','yp','r',
                                        'Lbatch','Nbatch','Nrepeat','tstall','seed','runtime'])
# print df_run.groupby('Nlay')['runtime'].min()
# print df_run.groupby('Nlay')['runtime'].max()

df_losses = pd.DataFrame(alllosses).unstack().reset_index().drop('level_10',axis=1)
df_losses.columns = ['Nlay','Nhid','xp','yp','r',
                     'Lbatch','Nbatch','Nrepeat','tstall','seed','losses']



# Get best loss for each run
df_losses['bestloss'] = df_losses.apply(lambda x : np.array(x['losses']).min(),axis=1)
df_losses['timetofive'] = df_losses.apply(lambda x : np.argmax(np.array(x['losses'])<1e-5),axis=1)
df_losses.loc[(df_losses['timetofive']==0),'timetofive'] = np.NaN
 
# Plot all seeds for each parameter pair
df_hyper = df_losses[(df_losses.Nlay==3)&(df_losses.Nhid==20)&
                    (df_losses.xp=='0.5')&(df_losses.yp=='0.5')&(df_losses.r=='0.05')]
for [Lbatch,Nrepeat],grp in df_hyper.groupby(['Lbatch','Nrepeat']):
    for key,row in grp.iterrows():
        plt.semilogy(row.losses,label=row.seed)
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.title('Lbatch = %d, Nrepeat = %d'%(Lbatch,Nrepeat))
    plt.legend(title='seed',fontsize=12,ncol=10,handlelength=1,borderpad=1,columnspacing=1)
    plt.tight_layout()
    plt.savefig('plots/compseed-%d-%d-hyper.png'%(Lbatch,Nrepeat),dpi=200)
    plt.close()

# Get best seeds for each parameter set
idx_bestcases = df_losses.groupby(
    ['Nlay','Nhid','xp','yp','r',
     'Lbatch','Nbatch','Nrepeat','tstall'])['bestloss'].transform(min) == df_losses['bestloss']
df_bests = df_losses[idx_bestcases]

# Plot stats for varying Lbatch
df_bests_Lbatch = df_bests[(df_losses.Nrepeat==100)&
                           (df_losses.Nlay==3)&(df_losses.Nhid==20)&
                           (df_losses.xp=='0.5')&(df_losses.yp=='0.5')&(df_losses.r=='0.05')]
plt.semilogy(df_bests_Lbatch.Lbatch,df_bests_Lbatch.bestloss,'k--*',ms=13)
plt.xlabel('Batch Size')
plt.ylabel('Best Loss Achieved')
plt.tight_layout()
plt.savefig('plots/bob-Lbatch.png',dpi=200)
plt.close()

df_fives_Lbatch = df_hyper[np.isnan(df_hyper['timetofive'])==False].groupby('Lbatch')['timetofive'].agg(['count','mean','std']).reset_index()
plt.errorbar(df_fives_Lbatch.Lbatch,df_fives_Lbatch['mean'],
             yerr=df_fives_Lbatch['std']/np.sqrt(df_fives_Lbatch['count']))
plt.xlabel('Batch Size')
plt.ylabel('Epochs for Loss to Beat $10^{-5}$')
plt.tight_layout()
plt.savefig('plots/time-to-five-Lbatch.png',dpi=200)
plt.close()


# Plot stats for varying Nrepeat
df_bests_Nrepeat = df_bests[(df_losses.Lbatch==10000)&
                           (df_losses.Nlay==3)&(df_losses.Nhid==20)&
                           (df_losses.xp=='0.5')&(df_losses.yp=='0.5')&(df_losses.r=='0.05')]
plt.semilogy(df_bests_Nrepeat.Nrepeat,df_bests_Nrepeat.bestloss,'k--*',ms=13)
plt.xlabel('Number of Repetitions Between Randomizations')
plt.ylabel('Best Loss Achieved')
plt.tight_layout()
plt.savefig('plots/bob-Nrepeat.png',dpi=200)
plt.close()

df_fives_Nrepeat = df_hyper[np.isnan(df_hyper['timetofive'])==False].groupby('Nrepeat')['timetofive'].agg(['count','mean','std']).reset_index()
plt.errorbar(df_fives_Nrepeat.Nrepeat,df_fives_Nrepeat['mean'],
             yerr=df_fives_Nrepeat['std']/np.sqrt(df_fives_Nrepeat['count']))
plt.xlabel('Number of Repetitions Between Randomizations')
plt.ylabel('Epochs for Loss to Beat $10^{-5}$')
plt.tight_layout()
plt.savefig('plots/time-to-five-Nrepeat.png',dpi=200)
plt.close()
