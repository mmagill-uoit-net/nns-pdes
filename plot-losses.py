import numpy as np
import pandas as pd
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from os import listdir

mpl.rcParams['font.size']=18
mpl.rcParams['figure.figsize']=(10,5)
mpl.rcParams['lines.linewidth']=3

alllosses = {}
lossdir = 'losses/'
runtimes = []
for lossfile in listdir(lossdir):
    # Load file
    losses = np.load(lossdir+lossfile)
    runtime = losses[-1]
    losses = losses[:-1]
    Nepos = losses.shape[0]
    Nlay = int(lossfile.split('-')[0])
    Nhid = int(lossfile.split('-')[1])
    xp = lossfile.split('-')[2]
    yp = lossfile.split('-')[3]
    r = lossfile.split('-')[4]
    Lbatch = int(lossfile.split('-')[5])
    Nbatch = int(lossfile.split('-')[6])
    Nrepeat = int(lossfile.split('-')[7])
    tstall = int(lossfile.split('-')[8])
    seed = int(lossfile.split('-')[-1].split('.')[0])
    alllosses[Nlay,Nhid,xp,yp,r,Lbatch,Nbatch,Nrepeat,tstall,seed] = [losses]
    runtimes.append([Nlay,Nhid,xp,yp,r,Lbatch,Nbatch,Nrepeat,tstall,seed,runtime/Nepos])
    # # Plot loss vs epochs
    # plt.semilogy(losses)
    # plt.xlabel('Epoch')
    # plt.ylabel('Loss')
    # plt.title('Runtime was %.1f seconds per epoch'%(runtime/Nepos))
    # plt.tight_layout()
    # plt.savefig('plots/loss-'+lossfile.replace('npy','png'),dpi=200)
    # plt.close()


# # Look at runtimes
# df_run = pd.DataFrame(runtimes,columns=['Nlay','Nhid','xp','yp','r',
#                                         'Lbatch','Nbatch','Nrepeat','tstall','seed','runtime'])
# print df_run.groupby('Nlay')['runtime'].min()
# print df_run.groupby('Nlay')['runtime'].max()

df_losses = pd.DataFrame(alllosses).unstack().reset_index().drop('level_10',axis=1)
df_losses.columns = ['Nlay','Nhid','xp','yp','r',
                     'Lbatch','Nbatch','Nrepeat','tstall','seed','losses']



# Get best loss for each run
df_losses['bestloss'] = df_losses.apply(lambda x : np.array(x['losses']).min(),axis=1)
df_losses['timetofive'] = df_losses.apply(lambda x : np.argmax(np.array(x['losses'])<1e-5),axis=1)
df_losses.loc[(df_losses['timetofive']==0),'timetofive'] = np.NaN
 
# Plot all seeds for each parameter pair
df_base = df_losses[(df_losses.Lbatch==10000)&(df_losses.Nbatch==10)&(df_losses.Nrepeat==100)&
                    (df_losses.xp=='0.5')&(df_losses.yp=='0.5')&(df_losses.r=='0.05')&
                    (df_losses.tstall==5)]
for [Nlay,Nhid],grp in df_base.groupby(['Nlay','Nhid']):
    for key,row in grp.iterrows():
        plt.semilogy(row.losses,label=row.seed)
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.title('Nlay = %d, Nhid = %d'%(Nlay,Nhid))
    plt.legend(title='seed',fontsize=12,ncol=10,handlelength=1,borderpad=1,columnspacing=1)
    plt.tight_layout()
    plt.savefig('plots/compseed-%d-%d-baseline.png'%(Nlay,Nhid),dpi=200)
    plt.close()

# Get best seeds for each parameter set
idx_bestcases = df_losses.groupby(
    ['Nlay','Nhid','xp','yp','r',
     'Lbatch','Nbatch','Nrepeat','tstall'])['bestloss'].transform(min) == df_losses['bestloss']
df_bests = df_losses[idx_bestcases]

# Plot stats for baseline model
df_bests_base = df_bests[(df_bests.Lbatch==10000)&(df_bests.Nbatch==10)&(df_bests.Nrepeat==100)&
                   (df_bests.xp=='0.5')&(df_bests.yp=='0.5')&(df_bests.r=='0.05')&
                   (df_bests.tstall==5)]
for Nlay,grp in df_bests_base.groupby('Nlay'):
    line = plt.loglog(grp.Nhid,grp.bestloss)
    plt.text(grp.Nhid.iloc[-1]-20,grp.bestloss.iloc[-1]*0.85,
             '$N_\mathrm{lay}=%d$'%Nlay,color=line[0].get_color(),fontsize=12)
df_bests_base_berg = pd.concat([df_bests_base[(df_bests_base.Nlay==1)&(df_bests_base.Nhid==120)],
                     df_bests_base[(df_bests_base.Nlay==2)&(df_bests_base.Nhid==20)],
                     df_bests_base[(df_bests_base.Nlay==3)&(df_bests_base.Nhid==14)],
                     df_bests_base[(df_bests_base.Nlay==4)&(df_bests_base.Nhid==12)],
                     df_bests_base[(df_bests_base.Nlay==5)&(df_bests_base.Nhid==10)]])
plt.loglog(df_bests_base_berg.Nhid,df_bests_base_berg.bestloss,'k--')
plt.text(15,df_bests_base.bestloss.max()*0.9,'Models of Comparable Capacity',rotation=6)
plt.xlim([9,132])
plt.ylim(ymin=df_bests_base.bestloss.min()*0.8)
plt.xlabel('Number of Neurons per Layer')
plt.ylabel('Best Loss Achieved')
plt.tight_layout()
plt.savefig('plots/bob-baseline.png',dpi=200)
plt.close()

# Look at time to reach 10^-5
df_fives = df_base[np.isnan(df_base['timetofive'])==False].groupby(['Nlay','Nhid'])['timetofive'].agg(['count','mean','std']).reset_index()
for Nlay,grp in df_fives.groupby('Nlay'):
    plt.errorbar(grp.Nhid,grp['mean'],yerr=grp['std']/np.sqrt(grp['count']),label='$N_\mathrm{lay}=%d$'%Nlay)
df_fives_berg = pd.concat([df_fives[(df_fives.Nlay==1)&(df_fives.Nhid==120)],
                           df_fives[(df_fives.Nlay==2)&(df_fives.Nhid==20)],
                           df_fives[(df_fives.Nlay==3)&(df_fives.Nhid==14)],
                           df_fives[(df_fives.Nlay==4)&(df_fives.Nhid==12)],
                           df_fives[(df_fives.Nlay==5)&(df_fives.Nhid==10)]])
plt.plot(df_fives_berg.Nhid,df_fives_berg['mean'],'k--',label='Similar\nCapacity')
plt.text(32,48,'Models of Comparable Capacity',rotation=7)
plt.annotate('Increasing $N_\mathrm{lay}$',xytext=(30,65),xy=(15,15),
             arrowprops=dict(arrowstyle="->",linewidth=2))
plt.ylim(ymin=0)
plt.xscale('log')
plt.xlabel('Number of Neurons per Layer')
plt.ylabel('Epochs for Loss to Beat $10^{-5}$')
plt.tight_layout()
plt.savefig('plots/time-to-five-baseline.png',dpi=200)
plt.close()
