from __future__ import print_function
import fenics as fnx
import mshr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

plt.rcParams['image.cmap'] = 'hot'


# Parameters
# Nx,Ny     # Resolution
# xp,yp     # Point source location
def SolvePotential(Nx=200,Ny=200,
                   xp=0.3,yp=0.5,r=0.1):

    # Create mesh
    pos_BL = fnx.Point(0.0,0.0)
    pos_UR = fnx.Point(1.0,1.0)
    mesh = fnx.RectangleMesh(pos_BL,pos_UR,Nx-1,Ny-1)
    
    # Define function space
    V = fnx.FunctionSpace(mesh, 'P', 1)
    
    # Define boundary conditions (Homogeneous Newmann BCs by default)
    def boundary(x, on_boundary):
        return on_boundary
    bc = fnx.DirichletBC(V, fnx.Constant(0.0), boundary)

    # Define source: disk of charge
    f = fnx.interpolate(
        fnx.Expression(
            '((x[0]-xp)*(x[0]-xp)+(x[1]-yp)*(x[1]-yp) <= r*r ) ? g : h',
            xp=xp, yp=yp, r=r, g=fnx.Constant(1.0), h=fnx.Constant(0.0),
            degree=1),
        V)
    
    # Define variational problem
    u = fnx.TrialFunction(V)
    v = fnx.TestFunction(V)
    #f = fnx.Constant(0.0)
    a = fnx.dot(fnx.grad(u), fnx.grad(v))*fnx.dx
    L = f*v*fnx.dx
    A, b = fnx.assemble_system(a,L,bc)
    
    # # Add point source
    # pos_Source = fnx.Point(xp,yp)
    # Source = fnx.PointSource(V, pos_Source, 1.0)
    # Source.apply(b)
    
    # Compute solution
    u = fnx.Function(V)
    fnx.solve(A, u.vector(), b)
    fig, ax = plt.subplots(figsize=(8,8))
    fnx.plot(u)
    ax.axis('off')
    ax.set_aspect('equal')
    plt.subplots_adjust(right=1.0,left=0.0,top=1.0,bottom=0.0)
    plt.show()
    
    # Format for naive storage (can improve format if memory becomes a problem)
    mesh_vals = mesh.coordinates()
    x = mesh_vals[:,0].reshape(-1)
    y = mesh_vals[:,1].reshape(-1)
    I = np.ones(x.shape)
    U = u.compute_vertex_values()
    return np.vstack([xp*I,yp*I,r*I,x,y,U]).T


# ### Create a dataset for (xp,yp,x,y,u)
# N = 6000
# Nx = 100
# Ny = 100
# r = 0.05
# data = []
# # Generate random source disks that don't cross the edge
# mybuffer = 0.01
# for i,[xp,yp] in enumerate(np.random.rand(N,2)*(1-(2+mybuffer)*r)+r):
#     A = SolvePotential(Nx=Nx,Ny=Ny,
#                        xp=xp,yp=yp,r=r)
#     data.append(A)
#     if i%100==0:
#         print(i)
    
# data = np.concatenate(data)
# np.save('newdata.npy',data)
