import pandas as pd
import numpy as np

df = pd.DataFrame(columns=['Nhid','Nlay','LambdaR','losses','Nonzeroweights','allweights','allbiases'])
df.loc[0] = [0,0,0,np.array([0,0]),np.array([0,0,0]),np.array([0,0,0,0]),np.array([0,0,0,0,0])]
df.to_pickle("data.pickle")
            
