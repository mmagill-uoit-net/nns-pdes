import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import sklearn.model_selection as sms
import sys
import graphviz as viz


plt.rcParams['image.cmap'] = 'hot'

keeprate = 0.99


Nres = 200
xx = np.linspace(0,1,Nres)
yy = np.linspace(0,1,Nres)
XX,YY = np.meshgrid(xx,yy)
I = np.ones(Nres**2).reshape(-1,1)
r_rep = 0.05
xp_rep = 0.5
yp_rep = 0.5
datax_rep = np.hstack([I*xp_rep,I*yp_rep,I*r_rep,np.array([XX,YY]).T.reshape(-1,2)])

def Benchmark(frame_num):
    fig, ax = plt.subplots(figsize=(8,8))
    predy_rep = sess.run(output,{x:datax_rep,keep_prob:1.0})
    ax.pcolor(XX,YY,predy_rep.reshape(Nres,Nres).T,vmin=0.0,vmax=1.0)
    ax.set_aspect('equal')
    ax.axis('off')
    plt.tight_layout()
    plt.subplots_adjust(bottom=0.0,top=1.0,left=0.0,right=1.0,wspace=0, hspace=0)
    plt.savefig('videos/benchmark-%s-%04d.png'%(casename,frame_num))
    plt.close()
    

def sigmoid(x):
    return 1./(1.+np.exp(-x))


def b2h(biases):
    h = 0.0*biases
    h[biases>0] = 0.0           # Red
    h[biases<0] = 0.67          # Blue
    return h
    
def b2s(biases):
    return (1./(1.+np.exp(-np.abs(biases)*50))-0.5)*2.0

def w2h(weights):
    h = 0.0*weights
    h[weights>0] = 0.0           # Red
    h[weights<0] = 0.67          # Blue
    return h

log10center = -1
log10width = 1

def w2s(weights):
    #return (1./(1.+np.exp(-np.abs(weights)*ampfac))-0.5)*2.0
    return sigmoid(log10width*(np.log10(np.abs(weights))-log10center))

def w2w(weights):
    #return 2.0*(1./(1.+np.exp(-np.abs(weights)*ampfac))-0.5)*2.0
    return 2.0*sigmoid(log10width*(np.log10(np.abs(weights))-log10center))

def w2as(weights):
    #return (1./(1.+np.exp(-np.abs(weights)*ampfac))-0.5)*2.0
    return 2.0*sigmoid(log10width*(np.log10(np.abs(weights))-log10center))

inputs = ['xp','yp','r','x','y']    

def MakeGraph(frame_num):

    # Make graph
    dot = viz.Digraph(format='png')
    dot.attr(rankdir='LR')
    dot.attr('node',shape='circle')

    # Update nodes
    node_hues = b2h(allbiases)
    node_saturations = b2s(allbiases)
    for i in range(1,N_lay):
        for j in range(N_hid):
            ind = 5+(i-1)*N_hid+j
            dot.node('L%02dN%02d'%(i,j),
                     label='',
                     style='filled',
                     fillcolor='%f %f 1.0'%(node_hues[ind],node_saturations[ind]))
    dot.node('out',
             label='u',
             style='filled',
             fillcolor='%f %f 1.0'%(node_hues[-1],node_saturations[-1]))
    
    # Update edges
    curweights = allweights.reshape(-1,int(N_hid))
    edge_hues = w2h(curweights)
    edge_saturations = w2s(curweights)
    edge_widths = w2w(curweights)
    edge_arrowsizes = w2as(curweights)
    for i,inp in enumerate(inputs):
        for j in range(N_hid):
            dot.edge(inp,'L%02dN%02d'%(1,j),
                     color='%f %f 1.0'%(edge_hues[i,j],edge_saturations[i,j]),
                     penwidth='%f'%edge_widths[i,j],
                     arrowsize='%f'%edge_arrowsizes[i,j])
    for i in range(1,N_lay-1):
        for j in range(N_hid):
            for k in range(N_hid):
                row = 5+(i-1)*N_hid+j
                dot.edge('L%02dN%02d'%(i,j),'L%02dN%02d'%(i+1,k),
                         color='%f %f 1.0'%(edge_hues[row,k],
                                            edge_saturations[row,k]),
                         penwidth='%f'%edge_widths[row,k],
                         arrowsize='%f'%edge_arrowsizes[row,k])
    for j in range(N_hid):
        dot.edge('L%02dN%02d'%(N_lay-1,j),'out',
                 color='%f %f 1.0'%(edge_hues[-1,j],
                                    edge_saturations[-1,j]),
                 penwidth='%f'%edge_widths[-1,j],
                 arrowsize='%f'%edge_arrowsizes[-1,j])
    
    dot.render('videos/graph-%s-%04d.gv'%(casename,frame_num),
               view=False, cleanup=True)



#########################################################################################

N_hid = 10
N_lay = 20
activator = tf.nn.relu

N_batches = 10000
L_batch = 1000

testfrac = 0.2
splitseed = 111

Nepo1 = 200
Nepo2 = 2000
Nfreq1 = 10000*3
Nfreq2 = 10000*3
Lrate = 0.1

Lambda_R = 0.000001

#optimizer = tf.train.GradientDescentOptimizer(Lrate)
optimizer = tf.train.AdamOptimizer()

lossname = 'MSE'

casename = (
    'video-' +
    '%d-%d-%s-' % (N_hid,N_lay,activator.func_name) +
    '%d-%d-'    % (N_batches,L_batch) +
    '%03d-%d-'   % (int(round(testfrac*100)),splitseed) +
    '%.0e-%s-%s-%d-%d-%.0e'%(Lrate,optimizer.get_name(),lossname,Nepo1,Nepo2,Lambda_R)
    )


#########################################################################################

# Load and normalize data
data = np.load('data-6000.npy')
data[:,5] /= data[:,5].max()
data = data[np.random.choice(data.shape[0],N_batches*L_batch,replace=False),:]
datax = data[:,0:5]
datay = data[:,5].reshape(-1,1)
print "Data loaded."

# Split into train-test, batches
datax_train, datax_test, datay_train, datay_test = sms.train_test_split(
    datax, datay, test_size = testfrac, random_state = splitseed)
batch_idxs = np.array_split(np.random.permutation(datax_train.shape[0]),N_batches)

# Define the network architecture
keep_prob = tf.placeholder(tf.float32)
x = tf.placeholder(tf.float32, [None,5])
hidden_weights = [tf.get_variable("a0",shape=[5, N_hid],initializer=tf.glorot_uniform_initializer())]
hidden_biases = [tf.Variable(tf.zeros([N_hid]))]
hidden_layers = [tf.nn.relu(tf.matmul(x,hidden_weights[0])+hidden_biases[0])]
for i in range(1,N_lay):
    hidden_weights.append(tf.nn.dropout(
        tf.get_variable("a%d"%i,shape=[N_hid, N_hid],initializer=tf.glorot_uniform_initializer()),
        keep_prob))
    hidden_biases.append(tf.Variable(tf.zeros([N_hid])))
    hidden_layers.append(tf.nn.relu(tf.matmul(hidden_layers[i-1],hidden_weights[i])+hidden_biases[i]))
output_weights = tf.get_variable("out",shape=[N_hid,1],initializer=tf.glorot_uniform_initializer())
output_biases = tf.Variable(tf.zeros([1]))
output = tf.matmul(hidden_layers[-1],output_weights)+output_biases

# Define learning parameters
y = tf.placeholder(tf.float32, [None,1])
loss = tf.reduce_mean(tf.square(output-y),name='MSE')

# Prepare to run
train = optimizer.minimize(loss)
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

# Run
losses = np.zeros(Nepo1+Nepo2)
Nonzeroweights = 0.0*losses
for i in range(Nepo1):
    for j in range(N_batches):
        sess.run(train, {x: datax_train[batch_idxs[j]],
                         y: datay_train[batch_idxs[j]],
                         keep_prob:keeprate})
        ind = (i*N_batches+j)
        if ind%Nfreq1==0:
            losses[i] = sess.run(loss,{x:datax_test,y:datay_test,keep_prob:1.0})
            allweights = np.concatenate([sess.run(w,{keep_prob:1.0}).reshape(-1) for w in hidden_weights+[output_weights]])
            allbiases = np.concatenate([sess.run(w,{keep_prob:1.0}).reshape(-1) for w in hidden_biases+[output_biases]])
            Nonzeroweights[i] = allweights[np.abs(allweights)>1e-4].shape[0]
            print "Epoch % 4d; Loss = %.2e; Nonzeroweights = %d"%(i,losses[i],Nonzeroweights[i])
            Benchmark(ind/Nfreq1)
            MakeGraph(ind/Nfreq1)
base_index = ind/Nfreq1
            
# Regularization: change target loss but still calculate L2 norm for losses plot
train = optimizer.minimize(loss + Lambda_R * tf.add_n([tf.reduce_sum(tf.abs(w)) for w in hidden_weights+[output_weights]]))
print "Start Regularizing."

# Run again
for i in range(Nepo1,Nepo1+Nepo2):
    for j in range(N_batches):
        sess.run(train, {x: datax_train[batch_idxs[j]],
                         y: datay_train[batch_idxs[j]],
                         keep_prob:keeprate})
        ind = (i*N_batches+j)
        if ind%Nfreq2==0:
            losses[i] = sess.run(loss,{x:datax_test,y:datay_test,keep_prob:1.0})
            allweights = np.concatenate([sess.run(w,{keep_prob:1.0}).reshape(-1) for w in hidden_weights+[output_weights]])
            allbiases = np.concatenate([sess.run(w,{keep_prob:1.0}).reshape(-1) for w in hidden_biases+[output_biases]])
            Nonzeroweights[i] = allweights[np.abs(allweights)>1e-4].shape[0]
            print "Epoch % 4d; Loss = %.2e; Nonzeroweights = %d -- Regularized"%(i,losses[i],Nonzeroweights[i])
            Benchmark(base_index+(ind-base_index)/Nfreq2)
            MakeGraph(base_index+(ind-base_index)/Nfreq2)
        

# # Save to pandas dataframe
# df = pd.read_pickle("data.pickle")
# df_new = pd.DataFrame(columns=df.columns)
# allbiases = np.concatenate([sess.run(w).reshape(-1) for w in hidden_biases+[output_biases]])
# df_new.loc[0] = [N_hid,N_lay,Lambda_R,losses,Nonzeroweights,allweights,allbiases]
# df = df.append(df_new,ignore_index=True)
# df.to_pickle("data.pickle")

#########################################################################################
#########################################################################################


