#!/bin/bash



# for Nhid in 5 10 20
# do
#     for Nlay in 5 10 20
#     do
# 	for LambdaR in 0.000001
# 	do
# 	    time python demo-learnG.py $Nhid $Nlay $LambdaR
# 	done
#     done
# done


## Didn't finish this one -- too regularized
# for Nhid in 10 20
# do
#     for Nlay in 5 10 20
#     do
# 	for LambdaR in 0.00001
# 	do
# 	    time python demo-learnG.py $Nhid $Nlay $LambdaR
# 	done
#     done
# done


# for Nhid in 10 20
# do
#     for Nlay in 5 10 20
#     do
# 	for LambdaR in 0.000003
# 	do
# 	    time python demo-learnG.py $Nhid $Nlay $LambdaR
# 	done
#     done
# done



# # Run with original regularization rate, but now double Nepo1 and Nepo2
# for Nhid in 10 20
# do
#     for Nlay in 5 10 20
#     do
# 	for LambdaR in 0.000001
# 	do
# 	    time python demo-learnG.py $Nhid $Nlay $LambdaR
# 	done
#     done
# done


# # Run again to get biases
# for Nhid in 10 20
# do
#     for Nlay in 5 10 20
#     do
# 	for LambdaR in 0.000001
# 	do
# 	    time python demo-learnG.py $Nhid $Nlay $LambdaR
# 	done
#     done
# done


# Run more
for Nhid in 10 20
do
    for Nlay in 5 10 20
    do
	for LambdaR in 0.000001
	do
	    time python demo-learnG.py $Nhid $Nlay $LambdaR
	done
    done
done
