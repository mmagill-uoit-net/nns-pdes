import numpy as np
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import sklearn.model_selection as sms
import sys


#########3 SAVING IS DISABLED
print "SAVING IS DISABLED!!!"

#########################################################################################

#N_hid = 10
#N_lay = 11
N_hid = int(sys.argv[1])
N_lay = int(sys.argv[2])
activator = tf.nn.tanh

N_batches = 10000
L_batch = 1000

testfrac = 0.2
splitseed = 111

Nepo1 = 50
Nepo2 = 2000*0
Lrate = 0.1

#Lambda_R = 0.000001
Lambda_R = float(sys.argv[3])

## Not currently optimizing any other optimizer hyperparameters!
optimizer = tf.train.GradientDescentOptimizer(Lrate)
#optimizer = tf.train.AdagradOptimizer(Lrate)
#optimizer = tf.train.AdamOptimizer(Lrate)
#optimizer = tf.train.AdadeltaOptimizer(Lrate)

lossname = 'MSE'
#lossname = 'TotalSE'
#lossname = 'WeightedMSE'

casename = (
    '%d-%d-%s-' % (N_hid,N_lay,activator.func_name) +
    '%d-%d-'    % (N_batches,L_batch) +
    '%03d-%d-'   % (int(round(testfrac*100)),splitseed) +
    '%.0e-%s-%s-%d-%d-%.0e'%(Lrate,optimizer.get_name(),lossname,Nepo1,Nepo2,Lambda_R)
    )

#########################################################################################

# Load and normalize data
data = np.load('data-6000.npy')
data[:,5] /= data[:,5].max()
data = data[np.random.choice(data.shape[0],N_batches*L_batch,replace=False),:]
datax = data[:,0:5]
datay = data[:,5].reshape(-1,1)
print "Data loaded."

# Split into train-test, batches
datax_train, datax_test, datay_train, datay_test = sms.train_test_split(
    datax, datay, test_size = testfrac, random_state = splitseed)
batch_idxs = np.array_split(np.random.permutation(datax_train.shape[0]),N_batches)

# Define the network architecture
x = tf.placeholder(tf.float32, [None,5])
hidden_weights = [tf.get_variable("a0",shape=[5, N_hid],initializer=tf.glorot_uniform_initializer())]
hidden_biases = [tf.Variable(tf.zeros([N_hid]))]
hidden_layers = [activator(tf.matmul(x,hidden_weights[0])+hidden_biases[0])]
for i in range(1,N_lay):
    hidden_weights.append(tf.get_variable("a%d"%i,shape=[N_hid, N_hid],initializer=tf.glorot_uniform_initializer()))
    hidden_biases.append(tf.Variable(tf.zeros([N_hid])))
    hidden_layers.append(activator(tf.matmul(hidden_layers[i-1],hidden_weights[i])+hidden_biases[i]))
output_weights = tf.get_variable("out",shape=[N_hid,1],initializer=tf.glorot_uniform_initializer())
output_biases = tf.Variable(tf.zeros([1]))
output = tf.matmul(hidden_layers[-1],output_weights)+output_biases

# Define learning parameters
y = tf.placeholder(tf.float32, [None,1])
if lossname=='MSE':
    loss = tf.reduce_mean(tf.square(output-y),name='MSE')
if lossname=='TotalSE':
    loss = tf.reduce_sum(tf.square(output-y),name='TotalSE')
if lossname=='WeightedMSE':
    loss = tf.reduce_mean(tf.square(output-y)*(1+y),name='WeightedMSE')

# Prepare to run
train = optimizer.minimize(loss)
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

# Run
losses = np.zeros(Nepo1+Nepo2)
Nonzeroweights = 0.0*losses
for i in range(Nepo1):
    for j in range(N_batches):
        sess.run(train, {x: datax_train[batch_idxs[j]],
                         y: datay_train[batch_idxs[j]]})
    if i%10==0:
        losses[i] = sess.run(loss,{x:datax_test,y:datay_test})
        allweights = np.concatenate([sess.run(w).reshape(-1) for w in hidden_weights+[output_weights]])
        Nonzeroweights[i] = allweights[np.abs(allweights)>1e-4].shape[0]
        print "Epoch % 4d; Loss = %.2e; Nonzeroweights = %d"%(i,losses[i],Nonzeroweights[i])

# # During histogram of weights
# plt.hist(np.log10(np.abs(np.concatenate([sess.run(w).reshape(-1) for w in hidden_weights+[output_weights]]))),100)
# plt.title('Weight Distribution During Training, Before Regularizing')
# plt.xlabel('Log10 of Abs of Weight Value')
# plt.tight_layout()
# plt.savefig('plots/during-%s.png'%casename)
# plt.close()

# Regularization: change target loss but still calculate L2 norm for losses plot
train = optimizer.minimize(loss + Lambda_R * tf.add_n([tf.reduce_sum(tf.abs(w)) for w in hidden_weights+[output_weights]]))
print "Start Regularizing."

# Run again
for i in range(Nepo1,Nepo1+Nepo2):
    for j in range(N_batches):
        sess.run(train, {x: datax_train[batch_idxs[j]],
                         y: datay_train[batch_idxs[j]]})
    if i%10==0:
        losses[i] = sess.run(loss,{x:datax_test,y:datay_test})
        allweights = np.concatenate([sess.run(w).reshape(-1) for w in hidden_weights+[output_weights]])
        Nonzeroweights[i] = allweights[np.abs(allweights)>1e-4].shape[0]
        print "Epoch % 4d; Loss = %.2e; Nonzeroweights = %d -- Regularized"%(i,losses[i],Nonzeroweights[i])

# # After histogram of weights
# plt.hist(np.log10(np.abs(np.concatenate([sess.run(w).reshape(-1) for w in hidden_weights+[output_weights]]))),100)
# plt.title('Weight Distribution After Training and Regularizing')
# plt.xlabel('Log 10 of Abs of Weight Value')
# plt.tight_layout()
# plt.savefig('plots/after-%s.png'%casename)
# plt.close()

# # Loss plot
# plt.semilogy(losses[losses>0],'.-')
# plt.title('Loss: %s'%loss.name)
# plt.ylabel('Loss')
# plt.xlabel('Checkpoint')
# plt.tight_layout()
# plt.savefig('plots/loss-%s.png'%casename)
# plt.close()
        
# # Nonzeroweights plot
# plt.semilogy(Nonzeroweights[Nonzeroweights>0],'.-')
# plt.ylabel('Number of Nonzero Weights')
# plt.xlabel('Checkpoint')
# plt.tight_layout()
# plt.savefig('plots/Nonzero-%s.png'%casename)
# plt.close()
        
# Replication test
xx = np.linspace(0,1,100)
yy = np.linspace(0,1,100)
XX,YY = np.meshgrid(xx,yy)
I = np.ones(10000).reshape(-1,1)
r_rep = datax[0,2]
datax_rep = np.hstack([I*0,I*0,I*r_rep,np.array([XX,YY]).T.reshape(-1,2)])
fig, axarr = plt.subplots(5,5,figsize=(8,8))
for i, axarr_i in enumerate(axarr):
    for j, ax in enumerate(axarr_i):
        xp_rep = (1-r_rep)-(1./4.)*(1-2*r_rep)*j
        yp_rep =   r_rep  +(1./4.)*(1-2*r_rep)*i
        datax_rep[:,0] = xp_rep
        datax_rep[:,1] = yp_rep
        predy_rep = sess.run(output,{x:datax_rep})
        axarr[i,j].pcolor(XX,YY,predy_rep.reshape(100,100).T,vmin=0.0,vmax=1.0)
        axarr[i,j].set_aspect('equal')
        axarr[i,j].axis('off')
plt.tight_layout()
plt.subplots_adjust(bottom=0.0,top=1.0,left=0.0,right=1.0,wspace=0, hspace=0)
plt.show()
# plt.savefig('plots/arraytest-%s.png'%casename)
# plt.close()

# # Save to pandas dataframe
# df = pd.read_csv("data.csv",index_col=0)
# df_new = pd.DataFrame(columns=df.columns)
# df_new.loc[0] = [N_hid,N_lay,Lambda_R,losses,Nonzeroweights,allweights]
# df = df.append(df_new,ignore_index=True)
# df.to_csv("data.csv")

# # Save to pandas dataframe
# df = pd.read_pickle("data.pickle")
# df_new = pd.DataFrame(columns=df.columns)
# allbiases = np.concatenate([sess.run(w).reshape(-1) for w in hidden_biases+[output_biases]])
# df_new.loc[0] = [N_hid,N_lay,Lambda_R,losses,Nonzeroweights,allweights,allbiases]
# df = df.append(df_new,ignore_index=True)
# df.to_pickle("data.pickle")

#########################################################################################
#########################################################################################


