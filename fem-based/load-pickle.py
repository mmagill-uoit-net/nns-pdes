import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams['font.size']=18
mpl.rcParams['figure.figsize']=(10,5)
mpl.rcParams['lines.linewidth']=3

# Load dataframe
#df = pd.read_csv('data.csv.bak',index_col=0)
df = pd.read_pickle('data.pickle')
df = df.drop([0])               # Dummy row
#df = df.drop([1,2])             # Bad runs -- will eventually remove these
# Track cases
Nhids = df.Nhid.unique().astype(int)
Nlays = df.Nlay.unique().astype(int)
LSs = ('-.','--','-')
MSs = ('o','v','s')

# Plot the loss vs nonzeroweights
lines_lay = [0]*3
lines_hid = [0]*3
for index, row in df.iterrows():
    # # Read from csv format
    # cur_losses = np.array(row.losses[2:-1].split()).astype(float)
    # cur_Nonzeroweights = np.array(row.Nonzeroweights[2:-2].split()).astype(float)
    # Read from pickle
    cur_losses = row.losses
    cur_Nonzeroweights = row.Nonzeroweights
    # Remove zero entries
    cur_losses = cur_losses[cur_losses>0]
    cur_Nonzeroweights = cur_Nonzeroweights[cur_Nonzeroweights>0]
    # Plot with colour by Nhid and linestyle by
    ind_lay = np.argmin(np.abs(row.Nlay-Nlays))
    ind_hid = np.argmin(np.abs(row.Nhid-Nhids))
    line_lay, = plt.loglog(cur_Nonzeroweights,cur_losses,
                           c='C%d'%(ind_lay),
                           ls=LSs[ind_lay])
    line_hid, = plt.loglog(cur_Nonzeroweights,cur_losses,
                           c='C%d'%(ind_lay),
                           ls=LSs[ind_lay],
                           marker=MSs[ind_hid])
    lines_lay[ind_lay] = line_lay      # Redundant but w.e
    lines_hid[ind_hid] = line_hid
#plt.xlabel('Number of weights above 1e-4')
plt.xlabel('Number of weights above $10^{-4}$')
plt.ylabel('Loss')
#legend1 = plt.legend(lines_lay, Nlays, title='Nlay', loc='upper right', handlelength=3)
legend1 = plt.legend(lines_lay, Nlays-1, title='Nlay', loc='upper right', handlelength=3)
plt.legend(lines_hid, Nhids, title='Nhid', loc='upper left', handlelength=1)
plt.gca().add_artist(legend1)
plt.tight_layout()
plt.savefig('loss-v-nonzero.png',dpi=200)
plt.close()


# Plot min loss curves
df['minloss'] = 0
for index, row in df.iterrows():
    # # Read from csv format
    # cur_losses = np.array(row.losses[2:-1].split()).astype(float)
    # Read from pickle
    cur_losses = row.losses
    # Remove zero entries
    cur_losses = cur_losses[cur_losses>0]
    df.loc[index,'minloss'] = cur_losses.min()
for key, grp in df.groupby('Nlay'):
    plt.semilogy(grp.Nhid,grp.minloss,label=str(int(key)))
plt.legend(title='Number of Layers')
plt.xlabel('Number of hidden units per layer')
plt.ylabel('Minimum loss')
plt.tight_layout()
plt.savefig('minloss.png',dpi=200)
plt.close()


# Plot inputs weights
for index, row in df.iterrows():
    cur_weights = row.allweights.reshape(-1,int(row.Nhid))

    # Compare x and xp
    xp_weights = cur_weights[0,:]
    x_weights = cur_weights[3,:]
    plt.plot(xp_weights,'-*',label='xp')
    plt.plot(x_weights,'-o',label='x')
    plt.plot(-xp_weights,'k--')
    plt.legend()
    plt.tight_layout()
    plt.savefig('plots/xpx-%d-%d-%.0e.png'%(int(row.Nhid),int(row.Nlay),row.LambdaR))
    plt.close()

    # Compare y and yp
    yp_weights = cur_weights[1,:]
    y_weights = cur_weights[4,:]
    plt.plot(yp_weights,'-*',label='yp')
    plt.plot(y_weights,'-o',label='y')
    plt.plot(-yp_weights,'k--')
    plt.legend()
    plt.savefig('plots/ypy-%d-%d-%.0e.png'%(int(row.Nhid),int(row.Nlay),row.LambdaR))
    plt.close()

    # Compare simultaneously
    fig, axarr = plt.subplots(2,1)
    axarr[0].pcolor(cur_weights[(0,1),:],
                    vmin=-1.5,vmax=1.5,cmap='bwr')
    axarr[1].pcolor(cur_weights[(3,4),:],
                    vmin=-1.5,vmax=1.5,cmap='bwr')
    plt.savefig('plots/xyxpyp-%d-%d-%.0e.png'%(int(row.Nhid),int(row.Nlay),row.LambdaR))
    plt.close()

    # Plot all weights
    myv = 1.5
    Nin = 1                     # Plot all inputs in one subplot
    Nout = 1                    # Plot outputs as one subplot
    Nlay = int(row.Nlay)        # One subplot per hidden layer
    Ntot = Nin + Nout + Nlay
    Nrows = int(np.floor(np.sqrt(Ntot)))
    Ncols = int(np.ceil(float(Ntot)/float(Nrows)))
    fig, axarr = plt.subplots(Nrows,Ncols,sharex=True)
    Nhid = int(row.Nhid)
    for i, ax in enumerate(axarr.flatten()):
        # Turn off tick labels
        ax.set_yticklabels([])
        ax.set_xticklabels([])
        if i==0:
            ax.pcolor(cur_weights[(0,1,2,3,4),:],vmin=-myv,vmax=myv,cmap='bwr')
        elif 5+(i-1)*Nhid<cur_weights.shape[0]:
            ax.pcolor(cur_weights[5+(i-1)*Nhid:5+i*Nhid],vmin=-myv,vmax=myv,cmap='bwr')
        else:
            ax.axis('off')
    plt.savefig('plots/allweights-%d-%d-%.0e.png'%(int(row.Nhid),int(row.Nlay),row.LambdaR))
    plt.close()

    # Figure out which nodes are alive
    fig, axarr = plt.subplots(2,1)
    axarr[0].set_yticklabels([])
    axarr[0].set_xticklabels([])
    axarr[0].pcolor((cur_weights[0:5,:]**2).sum(axis=1).reshape(1,-1),cmap='bone_r')
    axarr[1].set_yticklabels([])
    axarr[1].set_xticklabels([])
    axarr[1].pcolor(np.flipud((cur_weights[5:-1,:]**2).sum(axis=1).reshape(int(row.Nlay)-1,-1)),
                    cmap='bone_r')
    plt.savefig('plots/nodelife-%d-%d-%.0e.png'%(int(row.Nhid),int(row.Nlay),row.LambdaR))
    plt.close()

    # # Count active neurons per layer
    # # DEBUG THIS
    # thresh = 1e-4
    # inp_neurons = (cur_weights[0:5,:]**2).sum(axis=1).reshape(1,-1)
    # hid_neurons = np.flipud((cur_weights[5:-1,:]**2).sum(axis=1).reshape(int(row.Nlay)-1,-1))
    # inp_neurons[inp_neurons<thresh] = 0
    # hid_neurons[hid_neurons<thresh] = 0
    # Nalive = np.concatenate([np.array([np.count_nonzero(inp_neurons)]),
    #                          np.count_nonzero(hid_neurons,axis=1)])
    # plt.plot(Nalive)
    # plt.savefig('plots/Nalive-%d-%d-%.0e.png'%(int(row.Nhid),int(row.Nlay),row.LambdaR))
    # plt.close()
    
    
# Plot network










    
