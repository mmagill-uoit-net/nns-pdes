import numpy as np
import matplotlib as mpl
# mpl.use('Agg')
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import sklearn.model_selection as sms
import sys
import time


printfreq = 1

L_batch = int(sys.argv[7])      # Need large enough to exploit GPUs
N_repeat = int(sys.argv[8])     # Intended to improve gradient descent
N_batch = int( round(10.0*(10000.0/float(L_batch))*(100.0/float(N_repeat))) )
stallmax = 5

N_lay = int(sys.argv[1])
N_hid = int(sys.argv[2])
xp = tf.constant(float(sys.argv[3]))
yp = tf.constant(float(sys.argv[4]))
r  = tf.constant(float(sys.argv[5]))
seed = int(sys.argv[6])

casename = ( ('%d-%d-'%(N_lay,N_hid)) +
             ('%s-%s-%s-'%(sys.argv[3],sys.argv[4],sys.argv[5])) +
             ('%d-%d-%d-%d-'%(L_batch,N_batch,N_repeat,stallmax)) +
             ('%d'%seed) )

np.random.seed(seed)
tf.set_random_seed(seed)

#########################################################################################

activator = tf.nn.tanh
optimizer = tf.train.AdamOptimizer()

# Define the network architecture
x = tf.placeholder(tf.float32, [None])
y = tf.placeholder(tf.float32, [None])
hidden_weights = [tf.get_variable("a0",shape=[2, N_hid],initializer=tf.glorot_uniform_initializer())]
hidden_biases = [tf.Variable(tf.zeros([N_hid]))]
hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x,y])),hidden_weights[0])+hidden_biases[0])]
for i in range(1,N_lay):
    hidden_weights.append(tf.get_variable("a%d"%i,shape=[N_hid, N_hid],initializer=tf.glorot_uniform_initializer()))
    hidden_biases.append(tf.Variable(tf.zeros([N_hid])))
    hidden_layers.append(activator(tf.matmul(hidden_layers[i-1],hidden_weights[i])+hidden_biases[i]))
output_weights = tf.get_variable("out",shape=[N_hid,1],initializer=tf.glorot_uniform_initializer())
output_biases = tf.Variable(tf.zeros([1]))
output = tf.matmul(hidden_layers[-1],output_weights)+output_biases
output = tf.squeeze(output)

# Define learning parameters
u = x*(1.0-x)*y*(1.0-y) * output
uxx = tf.gradients(tf.gradients(u,x),x)[0]
uyy = tf.gradients(tf.gradients(u,y),y)[0]
source = tf.negative( tf.divide( tf.exp ( tf.divide(tf.negative(tf.square(x-xp) + tf.square(y-yp)),2*r) ), 2*np.pi*r))
loss = tf.reduce_mean(tf.square(uxx+uyy-source))

# Prepare to run
train = optimizer.minimize(loss)
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)
saver = tf.train.Saver()

#########################################################################################

bestloss=100.0
curtime=0
timesince=0
losses = []

def RunTraining():
    global bestloss,curtime,timesince,losses

    epochclock_start = time.time()
    
    # Train on random inputs
    for i in range(N_batch):    # Run over different datasets
       curx = np.random.rand(L_batch)
       cury = np.random.rand(L_batch)
       for j in range(N_repeat): # Run over the same dataset repeatedly
           sess.run(train, {x:curx,y:cury})

    # Get current loss
    testx = np.random.rand(L_batch)
    testy = np.random.rand(L_batch)
    curloss = sess.run(loss, {x:testx,y:testy})
    losses.append(curloss)

    # Track best loss so far
    if curloss < bestloss:
        bestloss = curloss
        timesince = 0
        saveclock_start = time.time()
        saver.save(sess, "models/%s.ckpt"%casename)
        save_elapsed = time.time() - saveclock_start
        print "Saving took %.3e seconds."%save_elapsed

    if curtime%printfreq==0:
        print "Epoch % 4d; Loss = %.2e; B.S.F. = %.2e; Epochs since B.S.F. = % 4d / % 4d"%(curtime,
                                                                                           curloss,bestloss,
                                                                                           timesince,stallmax)

    # Counters
    curtime=curtime+1
    timesince=timesince+1

    epoch_elapsed = time.time() - epochclock_start
    print "Epoch took %.3e seconds."%epoch_elapsed
    
    return timesince>stallmax

# Run
runclock_start = time.time()
while 1==1:
    endflag = RunTraining()
    if endflag:
        break
run_elapsed = time.time() - runclock_start
print "Total runtime was %.3e seconds in %d epochs."%(run_elapsed,curtime)

losses.append(run_elapsed)      # Very dirty workaround for now.
np.save('losses/%s.npy'%casename,np.array(losses))

