import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import sklearn.model_selection as sms
import sys
import time
from os import listdir

from cca_core import *

mpl.rcParams['font.size']=18
mpl.rcParams['figure.figsize']=(10,5)
mpl.rcParams['lines.linewidth']=3


refNlay = int(sys.argv[1])
refNhid = int(sys.argv[2])

nsamples = 500
#nsamples = 50
xx = np.linspace(0,1,nsamples)
yy = np.linspace(0,1,nsamples)
XX,YY = np.meshgrid(xx,yy)
testx = XX.flatten().astype(np.float32)
testy = YY.flatten().astype(np.float32)

def GetActivation(layer_num=0,
                  N_lay=1,N_hid=10,
                  xp=0.5,yp=0.5,r=0.05,
                  L_batch=10000,N_batch=10,N_repeat=100,stallmax=5,
                  seed=1):

    tf.reset_default_graph()
    
    casename = ( ('%d-%d-'%(N_lay,N_hid)) +
                 ('%s-%s-%s-'%(str(xp),str(yp),str(r))) +
                 ('%d-%d-%d-%d-'%(L_batch,N_batch,N_repeat,stallmax)) +
                 ('%d'%seed) )

    #########################################################################################
    
    activator = tf.nn.tanh
    optimizer = tf.train.AdamOptimizer()

    # Define the network architecture
    x = tf.placeholder(tf.float32, [None])
    y = tf.placeholder(tf.float32, [None])
    hidden_weights = [tf.get_variable("a0",shape=[2, N_hid],initializer=tf.glorot_uniform_initializer())]
    hidden_biases = [tf.Variable(tf.zeros([N_hid]))]
    hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x,y])),hidden_weights[0])+hidden_biases[0])]
    for i in range(1,N_lay):
        hidden_weights.append(tf.get_variable("a%d"%i,shape=[N_hid, N_hid],initializer=tf.glorot_uniform_initializer()))
        hidden_biases.append(tf.Variable(tf.zeros([N_hid])))
        hidden_layers.append(activator(tf.matmul(hidden_layers[i-1],hidden_weights[i])+hidden_biases[i]))
    output_weights = tf.get_variable("out",shape=[N_hid,1],initializer=tf.glorot_uniform_initializer())
    output_biases = tf.Variable(tf.zeros([1]))
    output = tf.matmul(hidden_layers[-1],output_weights)+output_biases
    output = tf.squeeze(output)
    
    # Define learning parameters
    u = x*(1.0-x)*y*(1.0-y) * output
    uxx = tf.gradients(tf.gradients(u,x),x)[0]
    uyy = tf.gradients(tf.gradients(u,y),y)[0]
    source = tf.negative( tf.divide( tf.exp ( tf.divide(tf.negative(tf.square(x-xp) + tf.square(y-yp)),2*r) ), 2*np.pi*r))
    loss = tf.reduce_mean(tf.square(uxx+uyy-source))
    
    # Prepare to run (don't need GPU)
    train = optimizer.minimize(loss)
    init = tf.global_variables_initializer()
    config = tf.ConfigProto(
        device_count = {'GPU': 0}
    )
    sess = tf.Session(config=config)
    #sess.run(init)
    saver = tf.train.Saver()
    saver.restore(sess, "models/%s.ckpt"%casename)

    #########################################################################################

    acts = sess.run(hidden_layers[layer_num],{x:testx,y:testy}).T
    testloss = sess.run(loss,{x:testx,y:testy}).T
    return acts, testloss





## Perform SVCCA between the first layers of all the best models on the baseline model

## First, find the seed coresponding to the best model for each (Nlay,Nhid)
alllosses = {}
lossdir = 'losses/'
for lossfile in listdir(lossdir):
    # Load file
    losses = np.load(lossdir+lossfile)
    runtime = losses[-1]
    losses = losses[:-1]
    Nepos = losses.shape[0]
    Nlay = int(lossfile.split('-')[0])
    Nhid = int(lossfile.split('-')[1])
    xp = lossfile.split('-')[2]
    yp = lossfile.split('-')[3]
    r = lossfile.split('-')[4]
    Lbatch = int(lossfile.split('-')[5])
    Nbatch = int(lossfile.split('-')[6])
    Nrepeat = int(lossfile.split('-')[7])
    tstall = int(lossfile.split('-')[8])
    seed = int(lossfile.split('-')[-1].split('.')[0])
    alllosses[Nlay,Nhid,xp,yp,r,Lbatch,Nbatch,Nrepeat,tstall,seed] = [losses]

df_losses = pd.DataFrame(alllosses).unstack().reset_index().drop('level_10',axis=1)
df_losses.columns = ['Nlay','Nhid','xp','yp','r',
                     'Lbatch','Nbatch','Nrepeat','tstall','seed','losses']

# Get best loss for each run
df_losses['bestloss'] = df_losses.apply(lambda x : np.array(x['losses']).min(),axis=1)

# Get best seeds for each parameter set
idx_bestcases = df_losses.groupby(
    ['Nlay','Nhid','xp','yp','r',
     'Lbatch','Nbatch','Nrepeat','tstall'])['bestloss'].transform(min) == df_losses['bestloss']
df_bests = df_losses[idx_bestcases]




# Isolate cases that vary xp
df_xp = df_losses[(df_losses.Nlay==refNlay)&(df_losses.Nhid==refNhid)&
                 (df_losses.Lbatch==10000)&(df_losses.Nbatch==10)&(df_losses.Nrepeat==100)&
                 (df_losses.yp=='0.5')&(df_losses.r=='0.05')&
                 (df_losses.tstall==5)]

## Compute and store all activations
def XpRowToActs(row):
    return [GetActivation(N_lay=int(row.Nlay),
                          N_hid=int(row.Nhid),
                          seed =int(row.seed),
                          xp   =float(row.xp),
                          layer_num=laynum)[0] for laynum in range(int(row.Nlay))]
df_xp['acts'] = df_xp.apply(XpRowToActs,axis=1)
idx_xp_bestcases = df_xp.groupby(
    ['Nlay','Nhid','xp','yp','r',
     'Lbatch','Nbatch','Nrepeat','tstall'])['bestloss'].transform(min) == df_xp['bestloss']
df_xp_bests = df_xp[idx_bestcases]


# Plot bob loss for each xp
plt.semilogy(df_xp_bests.xp,df_xp_bests.bestloss)
plt.xlabel('x position of the source')
plt.ylabel('Best Loss Achieved')
plt.tight_layout()
plt.savefig('plots/bob-xp-%d-%d.png'%(refNlay,refNhid),dpi=200)
plt.close()


# Compare all cases, not just the best, pairwise for layer-by-layer SVCCA
thresh = 0.99
for laynum in np.arange(refNlay):
    df_xp_tmp = df_xp[df_xp.Nlay>laynum]
    numrows = df_xp_tmp.shape[0]
    sims_xp = np.zeros([numrows]*2)
    for i in range(numrows):
        acts1 = df_xp_tmp.iloc[i].acts[laynum]
        for j in range(i,numrows):
            acts2 = df_xp_tmp.iloc[j].acts[laynum]
            cca_dict = get_cca_similarity(acts1,acts2,threshold=thresh,compute_dirns=False)
            avgcorr1 = cca_dict['sum'][0]
            avgcorr2 = cca_dict['sum'][1]
            avgcorr = max(avgcorr1,avgcorr2)
            sims_xp[i,j] = avgcorr
            sims_xp[j,i] = avgcorr
    fig, ax = plt.subplots(figsize=(4*2.54,3*2.54))
    heatmap = ax.pcolor(sims_xp)#,vmin=0.74,vmax=0.97)
    cbar = plt.colorbar(heatmap)
    # ax.grid(color='k', linestyle='-', linewidth=2)
    # ax.set_xticks(np.arange(10)*10)
    # ax.set_yticks(np.arange(10)*10)
    # ax.set_xticklabels(['',0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9])
    # ax.set_yticklabels(['',0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9])
    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_tick_params(left='off')
    ax.set_aspect('equal')
    plt.subplots_adjust(bottom=0.01,top=0.99)
    ax.set_title('SVCCA Similarities of Layer %d; Thresh %.0f%%'%(laynum+1,thresh*100))
    ax.set_xlabel('x position of the source')
    ax.set_ylabel('x position of the source')
    plt.savefig('plots/xp-thresh-%.2f-%d-%d-%d.png'%(thresh,refNlay,refNhid,laynum+1),dpi=200)
    plt.close()



    
# Isolate cases that vary r
df_r = df_losses[(df_losses.Nlay==refNlay)&(df_losses.Nhid==refNhid)&
                 (df_losses.Lbatch==10000)&(df_losses.Nbatch==10)&(df_losses.Nrepeat==100)&
                 (df_losses.yp=='0.5')&(df_losses.xp=='0.5')&
                 (df_losses.tstall==5)]

## Compute and store all activations
def RRowToActs(row):
    return [GetActivation(N_lay=int(row.Nlay),
                          N_hid=int(row.Nhid),
                          seed =int(row.seed),
                          r   =float(row.r),
                          layer_num=laynum)[0] for laynum in range(int(row.Nlay))]
df_r['acts'] = df_r.apply(RRowToActs,axis=1)
idx_r_bestcases = df_r.groupby(
    ['Nlay','Nhid','xp','yp','r',
     'Lbatch','Nbatch','Nrepeat','tstall'])['bestloss'].transform(min) == df_r['bestloss']
df_r_bests = df_r[idx_bestcases]



# Plot bob loss for each r
plt.semilogy(df_r_bests.r,df_r_bests.bestloss)
plt.xlabel('Radius of the source')
plt.ylabel('Best Loss Achieved')
plt.tight_layout()
plt.savefig('plots/bob-r-%d-%d.png'%(refNlay,refNhid),dpi=200)
plt.close()




# Compare all cases, not just the best, pairwise for layer-by-layer SVCCA
thresh = 0.99
for laynum in np.arange(refNlay):
    df_r_tmp = df_r[df_r.Nlay>laynum]
    numrows = df_r_tmp.shape[0]
    sims_r = np.zeros([numrows]*2)
    for i in range(numrows):
        acts1 = df_r_tmp.iloc[i].acts[laynum]
        for j in range(i,numrows):
            acts2 = df_r_tmp.iloc[j].acts[laynum]
            cca_dict = get_cca_similarity(acts1,acts2,threshold=thresh,compute_dirns=False)
            avgcorr1 = cca_dict['sum'][0]
            avgcorr2 = cca_dict['sum'][1]
            avgcorr = max(avgcorr1,avgcorr2)
            sims_r[i,j] = avgcorr
            sims_r[j,i] = avgcorr
    fig, ax = plt.subplots(figsize=(4*2.54,3*2.54))
    heatmap = ax.pcolor(sims_r)#,vmin=0.7,vmax=1.0)
    cbar = plt.colorbar(heatmap)
    # ax.grid(color='k', linestyle='-', linewidth=2)
    # ax.set_xticks(np.arange(6)*10)
    # ax.set_yticks(np.arange(6)*10)
    # ax.set_xticklabels(['',0.0125,0.025,0.05,0.1,0.2])
    # ax.set_yticklabels(['',0.0125,0.025,0.05,0.1,0.2])
    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_tick_params(left='off')
    ax.set_aspect('equal')
    plt.subplots_adjust(bottom=0.01,top=0.99)
    ax.set_title('SVCCA Similarities of Layer %d; Thresh %.0f%%'%(laynum+1,thresh*100))
    ax.set_xlabel('Radius of the source')
    ax.set_ylabel('Radius of the source')
    plt.savefig('plots/r-thresh-%.2f-%d-%d-%d.png'%(thresh,refNlay,refNhid,laynum+1),dpi=200)
    plt.close()


