import numpy as np
import matplotlib as mpl
# mpl.use('Agg')
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import sklearn.model_selection as sms
import sys
import time

# TODO:
# - Instead of printfreq, track the tic-toc and print if it has been some amount of time since the last print

###
### This file contains functions for my study of DENNs.
###


        
        # # Training parameters
        # self.size_batch_train = 200   # Number of sample points per training batch
        # self.size_batch_test = 1000   # Number of sample points per testing batch
        # self.batches_per_epoch = 1 # Number of training batches to generate between testing batch
        # self.trains_per_batch = 1 # Number of times to retrain on each generated training batch
        # self.stalls_to_end = 5    # If test loss fails to improve this many times, stop training

global curNeuralNetworkID = 0

class NeuralNetwork:

    def __init__(self,n_layers=1,
                 neurons_per_layer=5):

        # Network parameters
        self.n_layers = n_layers
        self.neurons_per_layer = neurons_per_layer
        self.activator = tf.nn.tanh
        self.initializer = tf.glorot_uniform_initializer()

        # Give the NeuralNetwork a unique ID
        global curNeuralNetworkID
        self.ID = curNeuralNetworkID
        self.name = "NN-%d"%self.ID
        curNeuralNetworkID = curNeuralNetworkID + 1
        
        # Build the network
        self.hidden_weights = [tf.get_variable("%s-0"%self.name,
                                               shape=[2, self.neurons_per_layer],
                                               initializer=self.initializer)]
        self.hidden_biases = [tf.Variable(tf.zeros([self.neurons_per_layer]))]
        self.hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x,y])),N_hidden_weights[0])+N_hidden_biases[0])]
        for i in range(1,self.n_layers):
            if i%L_res==0:
                N_hidden_weights.append(tf.get_variable("N%d"%i,shape=[num_hid, num_hid],initializer=tf.glorot_uniform_initializer()))
                N_hidden_biases.append(tf.Variable(tf.zeros([num_hid])))
                N_hidden_layers.append(activator(tf.matmul(N_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i])+
                                       N_hidden_layers[i-L_res])
            else:
                N_hidden_weights.append(tf.get_variable("N%d"%i,shape=[num_hid, num_hid],initializer=tf.glorot_uniform_initializer()))
                N_hidden_biases.append(tf.Variable(tf.zeros([num_hid])))
                N_hidden_layers.append(activator(tf.matmul(N_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
        N_output_weights = tf.get_variable("Nout",shape=[num_hid,1],initializer=tf.glorot_uniform_initializer())
        N_output_biases = tf.Variable(tf.zeros([1]))
        N_output = tf.matmul(N_hidden_layers[-1],N_output_weights)+N_output_biases
        N_output = tf.squeeze(N_output)




        
