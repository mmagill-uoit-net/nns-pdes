import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import sklearn.model_selection as sms
import sys
import time
from os import listdir

from cca_core import *


nsamples = 500
#nsamples = 50
xx = np.linspace(0,1,nsamples)
yy = np.linspace(0,1,nsamples)
XX,YY = np.meshgrid(xx,yy)
testx = XX.flatten().astype(np.float32)
testy = YY.flatten().astype(np.float32)

def GetActivation(layer_num=0,
                  N_lay=1,N_hid=10,
                  xp=0.5,yp=0.5,r=0.05,
                  L_batch=10000,N_batch=10,N_repeat=100,stallmax=5,
                  seed=1):

    tf.reset_default_graph()
    
    casename = ( ('%d-%d-'%(N_lay,N_hid)) +
                 ('%s-%s-%s-'%(str(xp),str(yp),str(r))) +
                 ('%d-%d-%d-%d-'%(L_batch,N_batch,N_repeat,stallmax)) +
                 ('%d'%seed) )

    #########################################################################################
    
    activator = tf.nn.tanh
    optimizer = tf.train.AdamOptimizer()

    # Define the network architecture
    x = tf.placeholder(tf.float32, [None])
    y = tf.placeholder(tf.float32, [None])
    hidden_weights = [tf.get_variable("a0",shape=[2, N_hid],initializer=tf.glorot_uniform_initializer())]
    hidden_biases = [tf.Variable(tf.zeros([N_hid]))]
    hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x,y])),hidden_weights[0])+hidden_biases[0])]
    for i in range(1,N_lay):
        hidden_weights.append(tf.get_variable("a%d"%i,shape=[N_hid, N_hid],initializer=tf.glorot_uniform_initializer()))
        hidden_biases.append(tf.Variable(tf.zeros([N_hid])))
        hidden_layers.append(activator(tf.matmul(hidden_layers[i-1],hidden_weights[i])+hidden_biases[i]))
    output_weights = tf.get_variable("out",shape=[N_hid,1],initializer=tf.glorot_uniform_initializer())
    output_biases = tf.Variable(tf.zeros([1]))
    output = tf.matmul(hidden_layers[-1],output_weights)+output_biases
    output = tf.squeeze(output)
    
    # Define learning parameters
    u = x*(1.0-x)*y*(1.0-y) * output
    uxx = tf.gradients(tf.gradients(u,x),x)[0]
    uyy = tf.gradients(tf.gradients(u,y),y)[0]
    source = tf.negative( tf.divide( tf.exp ( tf.divide(tf.negative(tf.square(x-xp) + tf.square(y-yp)),2*r) ), 2*np.pi*r))
    loss = tf.reduce_mean(tf.square(uxx+uyy-source))
    
    # Prepare to run (don't need GPU)
    train = optimizer.minimize(loss)
    init = tf.global_variables_initializer()
    config = tf.ConfigProto(
        device_count = {'GPU': 0}
    )
    sess = tf.Session(config=config)
    #sess.run(init)
    saver = tf.train.Saver()
    saver.restore(sess, "models/%s.ckpt"%casename)

    #########################################################################################

    acts = sess.run(hidden_layers[layer_num],{x:testx,y:testy}).T
    testloss = sess.run(loss,{x:testx,y:testy}).T
    return acts, testloss





## Perform SVCCA between the first layers of all the best models on the baseline model

## First, find the seed coresponding to the best model for each (Nlay,Nhid)
alllosses = {}
lossdir = 'losses/'
for lossfile in listdir(lossdir):
    # Load file
    losses = np.load(lossdir+lossfile)
    runtime = losses[-1]
    losses = losses[:-1]
    Nepos = losses.shape[0]
    Nlay = int(lossfile.split('-')[0])
    Nhid = int(lossfile.split('-')[1])
    xp = lossfile.split('-')[2]
    yp = lossfile.split('-')[3]
    r = lossfile.split('-')[4]
    Lbatch = int(lossfile.split('-')[5])
    Nbatch = int(lossfile.split('-')[6])
    Nrepeat = int(lossfile.split('-')[7])
    tstall = int(lossfile.split('-')[8])
    seed = int(lossfile.split('-')[-1].split('.')[0])
    alllosses[Nlay,Nhid,xp,yp,r,Lbatch,Nbatch,Nrepeat,tstall,seed] = [losses]

df_losses = pd.DataFrame(alllosses).unstack().reset_index().drop('level_10',axis=1)
df_losses.columns = ['Nlay','Nhid','xp','yp','r',
                     'Lbatch','Nbatch','Nrepeat','tstall','seed','losses']

# Get best loss for each run
df_losses['bestloss'] = df_losses.apply(lambda x : np.array(x['losses']).min(),axis=1)

# Get best seeds for each parameter set
idx_bestcases = df_losses.groupby(
    ['Nlay','Nhid','xp','yp','r',
     'Lbatch','Nbatch','Nrepeat','tstall'])['bestloss'].transform(min) == df_losses['bestloss']
df_bests = df_losses[idx_bestcases]


# Isolate baseline model
df_base = df_bests[(df_bests.Lbatch==10000)&(df_bests.Nbatch==10)&(df_bests.Nrepeat==100)&
                   (df_bests.xp=='0.5')&(df_bests.yp=='0.5')&(df_bests.r=='0.05')&
                   (df_bests.tstall==5)]

## Compute and store all activations
def BaseRowToActs(row):
    return [GetActivation(N_lay=int(row.Nlay),
                          N_hid=int(row.Nhid),
                          seed =int(row.seed),
                          layer_num=laynum)[0] for laynum in range(int(row.Nlay))]
df_base['acts'] = df_base.apply(BaseRowToActs,axis=1)


## Perform layer-wise SVCCA between all cases for baseline model
#thresh = 0.99
thresh = 0.90
for laynum in np.arange(5):
    df_base_tmp = df_base[df_base.Nlay>laynum]
    numrows = df_base_tmp.shape[0]
    sims_base = np.zeros([numrows]*2)
    for i in range(numrows):
        acts1 = df_base_tmp.iloc[i].acts[laynum]
        for j in range(i,numrows):
            acts2 = df_base_tmp.iloc[j].acts[laynum]
            cca_dict = get_cca_similarity(acts1,acts2,threshold=thresh,compute_dirns=False)
            avgcorr1 = cca_dict['mean'][0]
            avgcorr2 = cca_dict['mean'][1]
            avgcorr = max(avgcorr1,avgcorr2)
            sims_base[i,j] = avgcorr
            sims_base[j,i] = avgcorr
    fig, ax = plt.subplots(figsize=(4*2.54,3*2.54))
    heatmap = ax.pcolor(sims_base)#,vmin=0,vmax=1)
    cbar = plt.colorbar(heatmap)
    ax.grid(color='k', linestyle='-', linewidth=2)
    NumUniqLay = len(np.unique(df_base_tmp.Nlay))
    NumUniqHid = len(np.unique(df_base_tmp.Nhid))
    ax.set_xticks(NumUniqHid*np.arange(NumUniqLay))
    ax.set_yticks(NumUniqHid*np.arange(NumUniqLay))
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_tick_params(left='off')
    ax.set_aspect('equal')
    plt.subplots_adjust(bottom=0.01,top=0.99)
    ax.set_title('SVCCA Similarities of Layer %d in Baseline Model; Thresh %.0f%%'%(laynum+1,thresh*100))
    ax.set_xlabel('Major: Number of Layers; Minor: Neurons per Layer')
    ax.set_ylabel('Major: Number of Layers; Minor: Neurons per Layer')
    plt.savefig('plots/base-thresh-%.2f-simsbylayer-%d.png'%(thresh,laynum+1),dpi=200)
    plt.close()


