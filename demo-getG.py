import numpy as np
import matplotlib.pyplot as plt

### Generate solutions to the homogeneous Poisson equation on a square
### Source functions are delta functions in each mesh point

# GFL: http://tinyurl.com/y8gacyh7


# Parameters
L = 1.0                         # Length of box
W = L                           # Width of box
nmax = 400                      # Number of terms
xp = 0.5                        # x position of source
yp = 0.75                       # y position of source

# Mesh
res = 100
xx = np.linspace(0,L,res)
yy = np.linspace(0,W,res)
XX,YY = np.meshgrid(xx,yy)


# Make intermediate functions
def get_Pn(XX,xp,gamma_n):
    dXX_abs = np.abs(XX-xp)
    dXX_sum = XX+xp
    return ( (np.exp(-gamma_n*(2*L-dXX_abs)) - np.exp(-gamma_n*(2*L-dXX_sum)) +
              np.exp(-gamma_n*dXX_abs) - np.exp(-gamma_n*dXX_sum)) /
             (2*gamma_n*(1-np.exp(-2*gamma_n*L))) )

# Make Gns and sum to Gn
Ny = L/2.0
Gns = []
for n in range(1,nmax):
    gamma_n = n*np.pi/L
    Yn = np.sin(gamma_n*YY)
    Ynp = np.sin(gamma_n*yp)
    Pn = get_Pn(XX,xp,gamma_n)
    Gns.append(Yn*Ynp*Pn/Ny)
Gns = np.array(Gns)
Gn = Gns.sum(axis=0)


# Plot Gn
plt.pcolor(XX,YY,Gn)
plt.show()

# Test convergence
plt.plot(np.abs(np.cumsum(Gns,axis=0)-Gn).mean(axis=1).mean(axis=1))
plt.show()
